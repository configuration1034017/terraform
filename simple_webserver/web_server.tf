provider "aws" {
}

resource "aws_instance" "web_server" {
  ami = data.aws_ami.latest_ubuntu.id
  instance_type = "t2.micro"
  vpc_security_group_ids = [aws_security_group.my_webserver.id]  
  user_data = templatefile("bash_script.tpl", {
    f_name = "Andrii"
    l_name = "Petryshyn"
    names = ["Ira", "Mykola", "Oleh", "Andrii Petryshyn"]

  })
  tags = {
  Name = "Linux server"
  Owner = "Andrii Petryshyn"
  Project = "Terraform"
  }
} 

resource "aws_security_group" "my_webserver" {
  name        = "my_webserver"
  description = "Allow TLS inbound traffic"

  dynamic "ingress" {
    for_each = ["80", "443", "8080"]
    content {
    from_port        = ingress.value
    to_port           = ingress.value
    protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
    }
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  tags = {
   Name = "web_server_security_group"
   Owner = "Andrii Petryshyn"
  }
}

data "aws_ami" "latest_ubuntu" {
  owners = ["099720109477"]
  most_recent = true 
  filter {
    name = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }  
}

output "latest_ubuntu_ami_id" {
  value = data.aws_ami.latest_ubuntu.id

}


