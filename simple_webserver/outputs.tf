output "webserver_instance_id" {
  value = aws_instance.web_server.id
}

output "webserver_public_ip" {
    value = aws_instance.web_server.public_ip
}
