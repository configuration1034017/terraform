provider "aws" {
    region = "eu-west-2"
}

module "s3bucket" {
  source = "../modules/s3bucket/"
}
