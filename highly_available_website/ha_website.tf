# Highly available WebSite 
# Create:
#     - Security Group for Web server
#     - Launch Configuration with auto AMI Lookup
#     - Auto Scaling Group using 2 Availability Zones
#     - Classic Load Balancer in 2 Availability Zones

provider "aws" {
region = "eu-west-2"
  }

data "aws_availability_zones" "available" {}
data "aws_ami" "latest_ubuntu" {
  owners = ["099720109477"]
  most_recent = true 
  filter {
    name = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }  
}


resource "aws_security_group" "my_webserver" {
  name        = "my_webserver"
  description = "Allow inbound traffic"

  dynamic "ingress" {
    for_each = ["80", "443", "8080"]
    content {
    from_port        = ingress.value
    to_port           = ingress.value
    protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
    }
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  tags = {
   Name = "web_server_security_group"
   Owner = "Andrii Petryshyn"
  }
}

resource "aws_launch_configuration" "web" {
    name_prefix = "WebServer-Highly-Available-"
    image_id = data.aws_ami.latest_ubuntu.id
    instance_type = "t2.micro"
    security_groups = [aws_security_group.my_webserver.id]
    user_data = file("user_data.sh")

    lifecycle {
        create_before_destroy = true
        }
}


resource "aws_autoscaling_group" "web" {
    name = "ASG-${aws_launch_configuration.web.name}"
    launch_configuration = aws_launch_configuration.web.name
    min_size = 2
    max_size = 2
    min_elb_capacity = 2
    vpc_zone_identifier = [aws_default_subnet.default_az1.id, aws_default_subnet.default_az2.id]
    health_check_type = "ELB"
    load_balancers = [aws_elb.web_server.name]


    dynamic "tag" {
        for_each = {
            Name = "WebServer in ASG"
            Owner = "Andrii Petryshyn"
            TAGKEY = "TAGVALUE"
        }
    content {
      key = tag.key
      value = tag.value
      propagate_at_launch = true
    }
    } 
    lifecycle {
      create_before_destroy = true
    }
        }
resource "aws_elb" "web_server" {
  name               = "webserver-HA-ELB"
  availability_zones = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1]]
  security_groups    = [aws_security_group.my_webserver.id]

  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = 80
    instance_protocol = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 10
  }

  tags = {
    Name = "WebServer-Highly-Available-ELB"
  }
}

resource "aws_default_subnet" "default_az1" {
  availability_zone = data.aws_availability_zones.available.names[0]
}

resource "aws_default_subnet" "default_az2" {
  availability_zone = data.aws_availability_zones.available.names[1]
}

output "web_loadbalancer_url" {
    value = aws_elb.web_server.dns_name
  
}
