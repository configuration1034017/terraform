#!/bin/bash
sudo apt update
sudo apt install -y nginx
myip=`curl http://169.254.169.254/latest/meta-data/local-ipv4`
cat <<EOF > /var/www/html/index.html
<html>
<body bgcolor="black">
<h2><font color="gold">Build by Power of Terraform <font color="red"> v 1.4.6.</font></h2><br>
<font color="green"> Server PrivateIP: <font color="aqua">$myip<br><br>
<font color="magenta">
<b>Version 3.1</b>
</body>
</html>
EOF

sudo systemctl start nginx
sudo systemctl enable nginx

