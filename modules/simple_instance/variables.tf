variable "instance_type" {
    type = string
    default = "t2.micro" 
}

variable "image_id" {
    type = string
    default = "ami-0eb260c4d5475b901"
}

variable "allow_ports" {
    type = list
    default = ["80", "443", "8080"]  
}

variable "common_tags" {
    type = map
    default = {
      Owner = "Andrii Petryshyn"
      Project = "Module lesson"
  }
}

variable "root_block_size" {
  type = number
  default = 8
}

variable "root_block_type" {
    type = string
    default = "gp3"
  
}

variable "throughput" {
  type = number
  default = 153
  
}

variable "iops" {
  type = number
  default = 300
}

variable "tag_name" {
  type = string
  default = "Nginx"
}
