output "server_ami" {
    value = aws_instance.ubuntu.ami
}

output "security_groups" {
  value = aws_security_group.allow_ports.id

}
