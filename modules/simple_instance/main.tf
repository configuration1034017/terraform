resource "aws_instance" "ubuntu" {
    ami = var.image_id
    instance_type = var.instance_type
    vpc_security_group_ids = [aws_security_group.allow_ports.id]
    root_block_device {
      volume_size = var.root_block_size
      volume_type = var.root_block_type
      throughput = var.throughput
      iops = var.iops
    
    }
tags = merge(var.common_tags, { "Name" = "${var.tag_name}_instance_${timestamp()}"})

}

resource "aws_security_group" "allow_ports" {
    name = "Module security group"
    description = "Allow trafic for server"

    dynamic "ingress" {
        for_each = var.allow_ports
        content {
          from_port = ingress.value
          to_port = ingress.value
          protocol = "tcp"
          cidr_blocks = [ "0.0.0.0/0" ]
        }
    }
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    tags = merge(var.common_tags, { "Name" = "${var.tag_name}_security_group_${timestamp()}"})

}
