output "bucket_name" {
  value = aws_s3_bucket.data_storage.id
}

output "names_of_users" {
  value = [for user in aws_iam_user.username : user.name]
}

output "iam_policy" {
  value = aws_iam_policy.readonly_bucket_policy.id
  
}
