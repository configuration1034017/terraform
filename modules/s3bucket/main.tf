resource "random_string" "s3bucket_suffix" {
  length = 5
  special = false
  numeric = false
  upper = false
}

resource "aws_s3_bucket" "data_storage" {
  bucket = "${var.bucket_name}${random_string.s3bucket_suffix.result}"
  acl    = var.access_control_list

  tags = merge(var.common_tags, { "Name" = "${var.tag_name}_bucket_${timestamp()}"})
}

resource "aws_iam_user" "username" {
  for_each = toset(var.username)
  name     = each.value

  tags = merge(var.common_tags, { "Name" = "${var.tag_name}_user_${timestamp()}"})
}

resource "aws_iam_policy" "readonly_bucket_policy" {
  name   = "readonly_bucket_policy"
  description = "Read Only access policy"

policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": ${jsonencode(var.access)},
      "Resource": "arn:aws:s3:::${aws_s3_bucket.data_storage.bucket}/*"
    }
  ]
}
EOF

tags = merge(var.common_tags, { "Name" = "${var.tag_name}_iam_policy_${timestamp()}"})
}

resource "aws_iam_user_policy_attachment" "attach_policy_to_users" {
  for_each   = toset(var.username)
  user       = aws_iam_user.username[each.key].name
  policy_arn = aws_iam_policy.readonly_bucket_policy.arn

#  tags = merge(var.common_tags, { "Name" = "${var.tag_name}_policy_attachment_${timestamp()}"})
}

resource "aws_s3_object" "object1" {
for_each = fileset("myfiles/", "*")
bucket = aws_s3_bucket.data_storage.id
key = each.value
source = "myfiles/${each.value}"
etag = filemd5("myfiles/${each.value}")
}


