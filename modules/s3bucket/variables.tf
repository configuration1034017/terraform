variable "username" {
  type    = list  
  default = ["Andrii", "Arthur", "Jack"]
}

variable "access_control_list" {
  type    = string
  default = "private"
}

variable "bucket_name" {
  type    = string
  default = "bucccket"
}

variable "access" {
  type    = list
  default = ["s3:GetObject", "s3:ListBucket"]
}

variable "common_tags" {
    type = map
    default = {
      Owner = "Andrii Petryshyn"
      Project = "S3_project"
  }
}

variable "tag_name" {
  type = string
  default = "Nginx"
  
}

