variable "image_id" {
   default = "ami-0eb260c4d5475b901"
}

variable "instance_type" {
   default = "t2.micro" 
}

variable "allow_ports" {
  type = list
  default = ["80", "443", "8080", "22"]
}

variable "common_tags" {
  type = map
  default ={
    Owner = "Andrii Petryshyn"
    Project = "HA website"
    Environmet = "development"
  }
  
}
