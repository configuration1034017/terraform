provider "aws" { 
  region = "eu-west-2"
}

module "simple_instance" {
  source = "../modules/simple_instance/"
}
